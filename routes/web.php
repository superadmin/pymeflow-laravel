<?php

use Illuminate\Support\Facades\Route;
use SebaCarrasco93\PymeFlow\Config;
use SebaCarrasco93\PymeFlow\PymeFlow;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('php-plano')->group(function () {
	Route::get('create', function() {
		// Creación de la instancia
		$config = new Config(config('pymeflow.api_key'), config('pymeflow.secret_key'), config('pymeflow.api_url'), 'http://pymeflow-laravel.test/php-plano/confirm');
		$pymeflow = new PymeFlow($config);

		// Datos de compra
		$order = rand(1000, 2000);
        $subject = 'Pago de prueba con PHP Plano';
        $amount = 5000;
        $email = 'prueba@algo.com';
        $paymentMethod = 9;
        $optional = ['RUT' => '9999999-9', 'Otro dato' => 'Cualquier cosa'];

		// Crear la instancia
        $pymeflow->create($order, $subject, $amount, $email, $paymentMethod, $optional);
	});

	Route::post('confirm', function() {
		// Creación de la instancia
		$config = new Config(config('pymeflow.api_key'), config('pymeflow.secret_key'), config('pymeflow.api_url'), 'http://pymeflow-laravel.test/php-plano/confirm');
		$pymeflow = new PymeFlow($config);

		// Obtener el token enviado por POST
		$token = $_POST['token'];

		// Obtener resultado desde Flow (con detalle)
        $result = $pymeflow->result($token);
        // return $result;

        // Actualizar la información en la DB, mostrar el Voucher propio, etc...
        // ...

        // O también sólo obtener el estado en formato de texto
        $status = $pymeflow->status($token);
        // return $status;

        // O incluso, saber si fue pagada (devuelve true o false)
        $isPaid = $pymeflow->isPaid($token);
        return $isPaid;
	});
});

Route::get('/', function () {
    return view('welcome');
});
