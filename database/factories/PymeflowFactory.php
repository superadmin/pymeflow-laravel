<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use SebaCarrasco93\PymeFlow\Models\Pymeflow;
use Faker\Generator as Faker;

$factory->define(Pymeflow::class, function (Faker $faker) {
	$creation_date = $faker->date;

    return [
    	'token' => $faker->uuid(),
    	'flow_order' => $faker->randomNumber(5),
    	'commerce_order' => $faker->randomNumber(5),
    	'request_date' => $creation_date,
    	'status' => $faker->randomElement([1, 2, 3, 4]),
    	'subject' => $faker->word(10),
    	'currency' => 'CLP',
    	'amount' => $faker->randomNumber(5),
    	'payer' => $faker->email,
    	'optional' => [
    		'RUT' => $faker->randomNumber(8) . '-' . $faker->randomDigit,
    	],
    	'pending_info' => [
    		'media' => null,
    		'date' => null,
    	],
    	'payment_data' => [
    		'date' => null,
    		'media' => null,
    		'conversionDate' => null,
    		'conversionRate' => null,
    		'amount' => '5000.00',
    		'currency' => 'CLP',
    		'fee' => '145.00',
    		'taxes' => 28,
    		'balance' => 4827,
    		'transferDate' => now(),
    	],
    	'merchant_id' => null,
    	'created_at' => $creation_date,
    	'updated_at' => $creation_date,
    ];
});
