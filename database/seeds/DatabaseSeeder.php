<?php

use Illuminate\Database\Seeder;
use SebaCarrasco93\PymeFlow\Models\Pymeflow;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $pending = factory(Pymeflow::class, 8)->create(['status' => 1]);
        $paid = factory(Pymeflow::class, 10)->create(['status' => 2]);
        $rejected = factory(Pymeflow::class, 4)->create(['status' => 3]);
        $canceled = factory(Pymeflow::class, 2)->create(['status' => 4]);
    }
}
